## PoS-Tagger powered by Viterbi


#### Code Usage

- Important to have packages `nltk` and `conllu` installed (see [requirements.txt](https://git.informatik.uni-leipzig.de/dh55vita/hmm_pos/blob/master/requirements.txt) for details).
  - Using function `conllu.parse` to parse the data sets and function `word_tokenize` from `nltk.tokenize` to tokenize input as well as functions `accuracy`, `precision`, `recall` and `f_measure` from `nltk.metrics` to evaluate the tagger.

- For a demo `cd` to this directory and type:

```bash
python3 run.py
```

- For usage of tagger simply import the `HmmPos` class:

```python
from hmm_pos import HmmPos

hmm = HmmPos()

s = hmm.tag("Es ist immer noch Sommer!")

hmm.print_sentence(s)
Es/PRON
ist/AUX
immer/ADV
noch/ADV
Sommer/NOUN
!/PUNCT

hmm.eval()
```

#### Training Corpus

The [German UD](https://github.com/UniversalDependencies/UD_German-GSD) is converted from the content head version of the [universal
dependency treebank v2.0 (legacy)](https://github.com/ryanmcd/uni-dep-tb).

License: Creative Commons Attribution-NonCommercial-ShareAlike 3.0 United States


#### Universal PoS Tag Set

- `ADJ`: adjective
- `ADP`: adposition
- `ADV`: adverb
- `AUX`: auxiliary
- `CCONJ`: coordinating conjunction
- `DET`: determiner
- `NOUN`: noun
- `NUM`: numeral
- `PART`: particle
- `PRON`: pronoun
- `PROPN`: proper noun
- `PUNCT`: punctuation
- `SCONJ`: subordinating conjunction
- `SYM`: symbol
- `VERB`: verb
- `X`: other

#### File Format *.conllu

- `ID`: Word index, integer starting at 1 for each new sentence; may be a range for multiword tokens; may be a decimal number for empty nodes.
- `FORM`: Word form or punctuation symbol.
- `LEMMA`: Lemma or stem of word form.
- `UPOS`: Universal part-of-speech tag.
- `XPOS`: Language-specific part-of-speech tag; underscore if not available.
- `FEATS`: List of morphological features from the universal feature inventory or from a defined language-specific extension; underscore if not available.
- `HEAD`: Head of the current word, which is either a value of ID or zero (0).
- `DEPREL`: Universal dependency relation to the HEAD (root iff HEAD = 0) or a defined language-specific subtype of one.
- `DEPS`: Enhanced dependency graph in the form of a list of head-deprel pairs.
- `MISC`: Any other annotation.
