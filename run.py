import sys

try:
    import conllu
    import nltk
except ImportError:
    print("please install relevant packages before running this script!")
    print("see requirements.txt for more details.")
    sys.exit()

from hmm_pos import HmmPos

if __name__ == '__main__':

    print("welcome to the demo!")
    print("my name is markov.")
    print("and we are going to:")

    pos = HmmPos()

    print("evaluate the model:")

    pos.eval()

    print("now that we have that")

    print("what do you want me to tag?")

    s = '"Peter sieht den Mann mit dem Fernrohr und hört dabei Musik."'

    print(s)

    print("sure! let's do it!")

    tagged_sent = pos.tag(s)

    pos.print_sentence(tagged_sent)

    print("so, what do you think?")
