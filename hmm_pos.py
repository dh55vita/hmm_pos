import os
import sys
import json
import random

try:
    from conllu import parse
    from nltk.tokenize import word_tokenize
    from nltk.metrics import (accuracy,
                              precision,
                              recall,
                              f_measure)
except ImportError:
    print("please install relevant packages before running this script!")
    print("see requirements.txt for more details.")
    sys.exit()

class HmmPos:

    def __init__(self):

        self.paths = self.load_json("paths.json")

        self.train_file = os.path.realpath(self.paths['train'])
        self.test_file = os.path.realpath(self.paths['test'])

        print("read test data sets")
        self.train_raw = self.read_data(self.train_file)
        self.test_raw = self.read_data(self.test_file)
        self.raw = self.train_raw + self.test_raw

        print("randomize the data")
        random.shuffle(self.raw)
        split = int(len(self.raw) * 0.1)
        self.train = self.raw[split:]
        self.test = self.raw[:split]

        print("prepare the data")
        self.data = self.prepare_data(self.train)
        self.test_data = self.prepare_data(self.test)
        self.words_list = [word for sentence in self.data['words'] for word in sentence]
        self.tags_list = [tag for tagged_sent in self.data['tags'] for tag in tagged_sent]

        print("count frequencies")
        self.tag_freq = self.tag_frequency()
        self.bigram_freq = self.bigram_frequency()

        print("create the model")

        print("calculate start probability.")
        self.start_prob = self.start_probability()

        print("calculate transition probability..")
        self.trans_prob = self.trans_probability()

        print("calculate emission probability...")
        self.emiss_prob = self.emiss_probability()

    @staticmethod
    def load_json(path=""):
        try:
            f = open(path, 'r', encoding='utf-8')
        except:
            print("file",path,"could not be found.")
            print("please provide file paths.json with paths to conllu data sets.")
            sys.exit()
        with f:
            paths = json.load(f)
        return paths

    @staticmethod
    def read_data(path=""):
        try:
            f = open(path, 'r', encoding='utf-8')
        except IOError:
            print("file", path, "could not be found.")
            print("please correct path to conllu file in paths.json")
            sys.exit()
        with f:
            sentences = parse(f.read())
        return sentences

    @staticmethod
    def prepare_data(sentences):
        tags = []
        words = []
        vocab = set()
        tag_set = set()
        for sentence in sentences:
            tag = []
            word = []
            for token in sentence:
                form = token['form']  # .lower()
                pos = token['upostag']
                if pos == '_':  # or pos == 'X':
                    continue
                word.append(form)
                vocab.add(form)
                tag.append(pos)
                tag_set.add(pos)
            if word:
                if tag:
                    words.append(word)
                    tags.append(tag)
        tag_set = list(tag_set)
        tag_set.sort()
        return {"tags": tags,
                "words": words,
                "vocab": vocab,
                "tag_set": tag_set}

    def tag_frequency(self):
        tag_freq = {}
        for tag in self.tags_list:
            if tag not in tag_freq:
                tag_freq[tag] = 1
            else:
                tag_freq[tag] += 1
        return tag_freq

    def bigram_frequency(self):
        bigram_freq = {}
        for i in range(1, len(self.tags_list)):
            prev = self.tags_list[i-1]
            if prev in bigram_freq:
                table = bigram_freq[prev]
            else:
                bigram_freq[prev] = {}
                table = bigram_freq[prev]
            curr = self.tags_list[i]
            if curr in table:
                table[curr] += 1
            else:
                table[curr] = 1
        for t1 in self.data['tag_set']:
            for t2 in self.data['tag_set']:
                if t2 not in bigram_freq[t1]:
                    bigram_freq[t1][t2] = 0   # 1 / total ?
        return bigram_freq

    def bigram_probability(self, prev, tag):
        bi_freq = self.bigram_freq[prev][tag]
        uni_freq = self.tag_freq[prev]
        bi_prob = bi_freq / uni_freq
        return bi_prob

    def start_probability(self):
        start_prob = {}
        total = 0
        for tag_seq in self.data['tags']:
            first = tag_seq[0]
            if first not in start_prob:
                start_prob[first] = 0
            start_prob[first] += 1
            total += 1
        for tag in start_prob:
            count = start_prob[tag]
            prob = count / total
            start_prob[tag] = float('%.3g' % prob)
        for tag in self.data['tag_set']:
            if tag not in start_prob:
                start_prob[tag] = 0.0  # 1 / len(self.tags) # 0.0
        return start_prob

    def trans_probability(self):
        pos_prob = {}
        prev = 'PUNCT'
        for tag in self.tags_list:
            next_prob = self.bigram_probability(prev, tag)
            if prev in pos_prob:
                table = pos_prob[prev]
            else:
                table = {}
            table[tag] = float('%.3g' % next_prob)
            pos_prob[prev] = table
            prev = tag
        for t in self.data['tag_set']:
            if t not in pos_prob:
                pos_prob[t] = {}
            for t2 in self.data['tag_set']:
                if t2 not in pos_prob[t]:
                    pos_prob[t][t2] = 0.0
        return pos_prob

    def emiss_probability(self):
        word_prob = {}
        for i in range(len(self.words_list)):
            word = self.words_list[i]
            tag = self.tags_list[i]
            if tag in word_prob:
                table = word_prob[tag]
            else:
                word_prob[tag] = {}
                table = word_prob[tag]
            if word in table:
                table[word] = table[word] + 1
            else:
                table[word] = 1
        total = 0
        for tag in word_prob:
            for word in word_prob[tag]:
                occu = word_prob[tag]
                total = total + occu[word]
            for word in word_prob[tag]:
                occu = word_prob[tag]
                prob = occu[word] / total
                occu[word] = float('%.3g' % prob)
                word_prob[tag] = occu
            prob = 1 / total
            word_prob[tag]['unseen'] = float('%.3g' % prob)
            total = 0
        return word_prob

    def viterbi(self, words):
        vit = [{}]
        path = {}
        t = 0
        for tag in self.data['tag_set']:
            if words[t] in self.data['vocab']:
                if words[t] in self.emiss_prob[tag]:
                    vit[t][tag] = self.start_prob[tag] * self.emiss_prob[tag][words[0]]
                else:
                    vit[t][tag] = self.start_prob[tag] * 0.0
            else:
                vit[t][tag] = self.start_prob[tag] * self.emiss_prob[tag]['unseen']
            path[tag] = [tag]
        for t in range(1, len(words)):
            vit.append({})
            curr_path = {}
            for tag in self.data['tag_set']:
                if words[t] in self.data['vocab']:
                    if words[t] in self.emiss_prob[tag]:
                        prob, prev = max(
                            (vit[t - 1][prev_tag] * self.trans_prob[prev_tag][tag] * self.emiss_prob[tag][words[t]], prev_tag)
                            for prev_tag in self.data['tag_set'])
                    else:
                        prob, prev = max(
                            (vit[t - 1][prev_tag] * self.trans_prob[prev_tag][tag] * 0.0, prev_tag)
                            for prev_tag in self.data['tag_set'])
                else:
                    prob, prev = max(
                        (vit[t - 1][prev_tag] * self.trans_prob[prev_tag][tag] * self.emiss_prob[tag]['unseen'], prev_tag)
                        for prev_tag in self.data['tag_set'])
                vit[t][tag] = prob
                curr_path[tag] = path[prev] + [tag]
            path = curr_path
        prob, final = max((vit[t][tag], tag) for tag in self.data['tag_set'])
        return prob, path[final]

    def eval(self):
        acc = pre = rec = fm = total = 0
        for i, sentence in enumerate(self.test_data['words']):
            val, guess = self.viterbi(sentence)
            acc += accuracy(self.test_data['tags'][i], guess)
            guess = set(guess)
            tags = set(self.test_data['tags'][i])
            pre += precision(tags, guess)
            rec += recall(tags, guess)
            fm += f_measure(tags, guess)
            total += 1
        print("accuracy:", float('%.4g' % (100 * acc / total)), "%")
        print("precision:", float('%.4g' % (100 * pre / total)), "%")
        print("recall:", float('%.4g' % (100 * rec / total)), "%")
        print("f-measure:", float('%.4g' % (100 * fm / total)), "%")

    def tag(self, unlabeled):
        words = word_tokenize(unlabeled)
        prob, guess = self.viterbi(words)
        return [(words[i], guess[i]) for i in range(len(words))]

    @staticmethod
    def print_sentence(sentence):
        for form, pos in sentence:
            print(form+"/"+pos)
